class MyBigNumber {
  String addStrings(String s1, String s2) {
    // Kiểm tra nếu s1 hoặc s2 là null hoặc rỗng
    if (s1.isEmpty || s2.isEmpty) {
      throw Exception("Không thể cộng chuỗi rỗng.");
    }

    // Kiểm tra nếu s1 hoặc s2 không phải là chuỗi số => Không kiểm tra đầu vào
    // try {
    //   int.parse(s1);
    //   int.parse(s2);
    // } catch (e) {
    //   throw Exception("Chuỗi không hợp lệ.");
    // }
    //Bước 1: Xác định độ dài của chuỗi s1 và s2, và tìm độ dài lớn nhất giữa 2 chuỗi:
    int length1 = s1.length;
    int length2 = s2.length;
    int maxLength = length1 > length2 ? length1 : length2;

    int mem = 0;
    String resultTemp = "";

    //Bước 2: Duyệt từng ký tự của chuỗi dài nhất:
    for (int i = 0; i < maxLength; i++) {
      //Bước 3: Xác định chỉ số i1 và i2 đánh dấu ký tự cần lấy của chuỗi s1 và s2:
      int index1 = length1 - i - 1;
      int index2 = length2 - i - 1;

      //Bước 4: Lấy ra ký tự tại vị trí i1, i2 tương ứng của chuỗi s1 và s2:
      String c1 = index1 >= 0 ? s1[index1] : '0';
      String c2 = index2 >= 0 ? s2[index2] : '0';

      // Kiểm tra nếu s1 hoặc s2 chứa các ký tự không hợp lệ
      if (!RegExp(r'^\d$').hasMatch(c1) || !RegExp(r'^\d$').hasMatch(c2)) {
        throw Exception("Chuỗi không hợp lệ.");
      }

      //Bước 5: Xác định giá trị tương ứng của ký tự c1, c2:
      int d1 = int.parse(c1);
      int d2 = int.parse(c2);

      //Bước 6: Thực hiện cộng 2 ký số d1 và d2:
      int sum = d1 + d2 + mem;

      //Bước 7: Thực hiện lấy số hàng đơn vị của tổng sum và ghi nhận cờ nhớ
      int result = sum % 10;
      mem = sum ~/ 10;

      //Bước 8: Ghép kết quả result vào kết quả cuối cùng
      resultTemp = result.toString() + resultTemp;
    }

    //Bước 9: Khi kết thúc vòng lặp ở trên nếu biến nhớ mem có giá trị lớn hơn 0 thì ghép tiếp vào kết quả cuối cùng
    if (mem > 0) {
      resultTemp = mem.toString() + resultTemp;
    }

    return resultTemp;
  }
  

  
}
