# Yêu cầu
Dart SDK 2.13.0 hoặc mới hơn
Trình biên dịch Dart


## Để người dùng tải về mã nguồn từ GitHub và lưu vào thư mục (Ví dụ D:\RunNow), bạn có thể thực hiện các bước sau:
1. Truy cập trang GitHub repository chứa mã nguồn.
2. Nhấn vào nút "Code" để hiển thị các tùy chọn tải về.
3. Chọn "Download ZIP" để tải về tệp ZIP của mã nguồn.
4. Giải nén tệp ZIP vào thư mục D:\RunNow.
5. Sau khi mã nguồn đã được giải nén vào thư mục D:\RunNow, người dùng có thể mở command prompt hoặc terminal và đứng tại thư mục đã giải nén để chạy mã nguồn.

### Ví dụ, để chạy một tệp mã nguồn có tên "main.dart", người dùng có thể thực hiện các bước sau:
1. Mở command prompt hoặc terminal.
2. Sử dụng lệnh "cd" để điều hướng đến thư mục D:\RunNow. Ví dụ: cd D:\RunNow  (Trong project này sẽ là D:\RunNow\test)
3. Sau đó, sử dụng lệnh "dart" để chạy tệp mã nguồn. Ví dụ: dart main.dart      (Trong Project này sẽ là dart test_flutter_test.dart)


## Hướng dẫn biên dịch và chạy chương trình vscode
1. Cài đặt Dart SDK từ trang web chính thức của Dart.
2. Clone hoặc tải xuống dự án từ GitHub.
3. Mở terminal và di chuyển đến thư mục chứa mã nguồn của chương trình.
5. Gõ 'flutter test' 
Nếu 'All tests passed!' thì test thành công, nếu chuỗi không hợp lệ thì sẽ có thông báo "chuỗi không hơp lệ"

