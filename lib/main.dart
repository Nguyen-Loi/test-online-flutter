import 'package:test_flutter/my_big_number.dart';

String getTotal({required String str1, required String str2}) {
  MyBigNumber myBigNumber = MyBigNumber();
  String result = myBigNumber.addStrings(str1, str2);
  return result;
}
