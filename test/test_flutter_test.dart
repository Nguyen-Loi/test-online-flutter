import 'package:test_flutter/main.dart';
import 'package:test/test.dart';

void main() {
  test('Test case 1', () {
    expect(getTotal(str1: '123', str2: '456'), equals('579'));
  });

  test('Test case 2', () {
    expect(getTotal(str1: '999', str2: '1'), equals('1000'));
  });

  test('Test case 3', () {
    expect(
        getTotal(str1: '123456789', str2: '987654321'), equals('1111111110'));
  });
}
